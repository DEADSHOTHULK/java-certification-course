package codingExercise45;

import java.util.ArrayList;

public class Bank {
	private String name;
	private ArrayList<Branch> branches;
	
	public Bank(String name) {
		this.name = name;
		this.branches = new ArrayList<Branch>();
	}
	
	public boolean addBranch(String nameOfBranch) {
		Branch branch = findBranch(nameOfBranch);
		if(branch==null) {
			branches.add(new Branch(nameOfBranch));
			return true;
		}
		return false;
	}
	
	public boolean addCustomer(String nameOfBranch, String nameOfCustomer, double transaction) {
		Branch branch = findBranch(nameOfBranch);
		if(branch==null)
			return false;
		return branch.newCustomer(nameOfCustomer, transaction);
	}
	
	public boolean addCustomerTransaction(String nameOfBranch, String nameOfCustomer, double transaction) {
		Branch branch = findBranch(nameOfBranch);
		if(branch == null)
			return false;
		Customer customer = null;
		for(int i=0;i<branch.getCustomers().size();i++)
			if(branch.getCustomers().get(i).getName().equals(nameOfCustomer)) {
				customer = branch.getCustomers().get(i);
				break;
			}
		if(customer == null)
			return false;
		customer.addTransaction(transaction);
		return true;
	}
	
	private Branch findBranch(String name) {
		for(Branch branch : branches) {
			if(branch.getName().equals(name))
				return branch;
		}
		return null;
	}
	
	public boolean listCustomers(String name, boolean printTransaction) {
		Branch branch = findBranch(name);
		if(branch == null)
			return false;
		System.out.println("Customer details for branch "+name);
		for(int i=0;i<branch.getCustomers().size();i++) {
			Customer customer = branch.getCustomers().get(i);
			System.out.println("Customer: " + customer.getName()+"["+(i+1)+"]");
			if(printTransaction) {
				System.out.println("Transactions");
				for(int j=0;j<customer.getTransactions().size();j++) {
					double transaction = customer.getTransactions().get(j);
					System.out.println("["+(j+1)+"]  Amount "+transaction);
				}
			}
		}
		
		return true;
	}
}
