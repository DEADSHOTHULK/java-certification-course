package codingExercise46;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
	private String name;
	private String artist;
	private ArrayList<Song> songs;
	
	public Album(String name, String artist) {
		this.name = name;
		this.artist = artist;
		this.songs = new ArrayList<Song>();
	}
	
	public boolean addSong(String title, double duration) {
		Song song = findSong(title);
		if(song==null){
			songs.add(new Song(title, duration));
			return true;
		}
		return false;
	}
	
	public boolean addToPlayList(String title, LinkedList<Song> playlist) {
		Song song = findSong(title);
		if(song==null)
			return false;
		playlist.add(song);
		return true;
	}
	
	public boolean addToPlayList(int trackNum, LinkedList<Song> playlist) {
		if(trackNum>=1 && trackNum<=songs.size()) {
			Song song = songs.get(trackNum-1);
			playlist.add(song);
			return true;
		}
		return false;
	}
	
	private Song findSong(String name) {
		for(Song song : songs) {
			if(song.getTitle().equals(name))
				return song;
		}
		return null;
	}
}
