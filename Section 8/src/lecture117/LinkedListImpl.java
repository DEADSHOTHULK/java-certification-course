package lecture117;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListImpl {
	public static void main(String args[]) {
		LinkedList<String> cities = new LinkedList();
		addToListInOrder(cities, "Deoghar");
		addToListInOrder(cities, "Allahabad");
		addToListInOrder(cities, "Aambagh");
		addToListInOrder(cities, "Jodhpur");
		addToListInOrder(cities, "Deoghar");
		addToListInOrder(cities, "Dumka");
		addToListInOrder(cities, "Dhanbad");
		printList(cities);
		
	}
	
	public static void printList(LinkedList<String> linkedList) {
		Iterator<String> iterator = linkedList.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
	
	public static boolean addToListInOrder(LinkedList<String> linkedList, String newCity) {
		ListIterator<String> listIterator = linkedList.listIterator();
		
		while(listIterator.hasNext()) {
			int comparator = listIterator.next().compareTo(newCity);
			if(comparator>0) {
				listIterator.previous();
				listIterator.add(newCity);
				return true;
			}
			else if(comparator==0) {
				System.out.println("ALready added");
				return false;
			}
		}
//		All the cities are smaller. So needs to be added at the end of the list
		listIterator.add(newCity);
		return true;
	}

}
