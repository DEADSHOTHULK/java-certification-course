package codingExercise44;

import java.util.ArrayList;

public class MobilePhone {

	private String myNumber;
	private ArrayList<Contact> myContacts;
	
	public MobilePhone(String myNumber) {
		this.myNumber = myNumber;
		this.myContacts = new ArrayList<Contact>();
	}
	
	public boolean addNewContact(Contact contact) {
		int indexOfContact = findContact(contact);
		if(indexOfContact>=0)
			return false;
		else {
			myContacts.add(contact);
			return true;
		}
	}
	
	public boolean updateContact(Contact oldContact, Contact newContact) {
		int indexOfContact = findContact(oldContact);
		if(indexOfContact>=0) {
			this.myContacts.set(indexOfContact, newContact);
			return true;
		}
		return false;
	}
	
	public boolean removeContact(Contact contact) {
		int indexOfContact = findContact(contact);
		if(indexOfContact>=0) {
			this.myContacts.remove(indexOfContact);
			return true;
		}
		return false;
	}
	
	private int findContact(Contact contact) {
		for(int i=0;i<myContacts.size();i++) {
			if(myContacts.get(i).getName().equals(contact.getName()))
				return i;
		}
		return -1;
	}
	
	private int findContact(String name) {
		for(int i=0;i<myContacts.size();i++) {
			if(myContacts.get(i).getName().equals(name))
				return i;
		}
		return -1;
	}
	
	public Contact queryContact(String name) {
		int indexOfContact = findContact(name);
		if(indexOfContact>=0)
			return myContacts.get(indexOfContact);
		return null;
	}
	
	public void printContacts() {
		System.out.println("Contact List:");
		for(int i=0;i<myContacts.size();i++) {
			System.out.println((i+1)+". "+myContacts.get(i).getName()+" -> "+myContacts.get(i).getPhoneNumber());
		}
	}
	
}
