package codingExercise41;

public class Main {
    public static void main(String[] args) {
        SortedArray sortedArray = new SortedArray();
        int array[] = sortedArray.getIntegers(5);
        array = sortedArray.sortIntegers(array);
        sortedArray.printArray(array);

        array = sortedArray.getIntegers(6);
        array = sortedArray.sortIntegers(array);
        sortedArray.printArray(array);
    }
}
