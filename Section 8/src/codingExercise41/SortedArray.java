package codingExercise41;

import java.util.Arrays;
import java.util.Scanner;

public class SortedArray {
    private static Scanner scanner = new Scanner(System.in);

    public static int[] getIntegers(int size){
//        Scanner scanner = new Scanner(System.in);
        int[] array = new int[size];
        for(int i=0;i<size;i++){
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static void printArray(int[] array){
        for(int i=0;i<array.length;i++){
            System.out.println("Element "+i+" contents "+array[i]);
        }
    }

    public static int[] sortIntegers(int[] array){
//        int[] sortedArray = Arrays.copyOf(array,array.length);
        boolean isSorted = false;
        while(!isSorted){
            isSorted = true;
            for(int i=0;i<array.length-1;i++){
                if(array[i]<array[i+1]){
                    isSorted = false;
                    array[i] = array[i]+array[i+1];
                    array[i+1] = array[i] - array[i+1];
                    array[i]-=array[i+1];
                }
            }
        }
        return array;
    }
}
