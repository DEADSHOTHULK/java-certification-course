public class CodingExercise1 {
    public static long toMilesPerHour(double kilometersPerHour) {
        long milesPerHour = 0;
        if (kilometersPerHour < 0) {
            milesPerHour = -1;
        } else {
            milesPerHour = Math.round(kilometersPerHour/1.609);
        }
        return milesPerHour;
    }

    public static void printConversion(double kilometersPerHour){
        long milesPerHour = toMilesPerHour(kilometersPerHour);
        if(milesPerHour<0)
            System.out.println("Invalid Value");
        else
            System.out.println(kilometersPerHour + " km/h = "+milesPerHour+" mi/h");
    }

    public static void main(String[] args) {
        printConversion(-90);
        printConversion(1);
        printConversion(75.114);
    }
}
