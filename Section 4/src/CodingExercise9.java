public class CodingExercise9 {
    public static void printYearsAndDays(long minutes){
        String result = "";
        if(minutes<0)
            result = "Invalid Value";
        else{
            long numberOfMinutesInDay = 60*24;
            long days = minutes / (numberOfMinutesInDay);
            long years = days/365;
            long remainingDays = days % 365;
            result = minutes + " min = " + years + " y and " + remainingDays + " d";
        }
        System.out.println(result);
    }

    public static void main(String[] args) {
        printYearsAndDays(525600);
        printYearsAndDays(1051200);
        printYearsAndDays(561600);
    }
}
