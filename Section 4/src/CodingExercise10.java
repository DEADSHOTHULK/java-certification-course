public class CodingExercise10 {
    public static void printEqual(int firstNum, int secondNum, int thirdNum){
        String result = "";
        if(firstNum<0 || secondNum<0 || thirdNum<0)
            result = "Invalid Value";
        else if(firstNum==secondNum && firstNum==thirdNum)
            result = "All numbers are equal";
        else if(firstNum!=secondNum && firstNum!=thirdNum && secondNum!=thirdNum){
            result = "All numbers are different";
        }else{
            result = "Neither all are equal or different";
        }
        System.out.println(result);
    }

    public static void main(String[] args) {
        printEqual(1,1,1);
        printEqual(1,1,2);
        printEqual(-1,-1,-1);
        printEqual(1,2,3);
    }
}
