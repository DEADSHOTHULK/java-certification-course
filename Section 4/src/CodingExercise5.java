public class CodingExercise5 {
    public static boolean areEqualByThreeDecimalPlaces(double firstNumber, double secondNumber){
        long firstNumberRounded = (long)(firstNumber*1000d);
        long secondNumberRounded = (long)(secondNumber*1000d);
        if(firstNumberRounded == secondNumberRounded)
            return true;
        return false;
    }

    public static void main(String[] args) {
        System.out.println(areEqualByThreeDecimalPlaces(-3.1756, -3.175));
        System.out.println(areEqualByThreeDecimalPlaces(3.1756, 3.176));
        System.out.println(areEqualByThreeDecimalPlaces(3.0, 3.0));
        System.out.println(areEqualByThreeDecimalPlaces(-3.123, 3.123));
    }
}
