public class CodingExercise8 {
    public static double area(double radius){
        double result;
        if(radius<0)
            result = -1.0;
        else
            result = (Math.PI*radius*radius);
        return result;
    }

    public static double area(double x, double y){
        double result;
        if(x<0 || y<0)
            result = -1.0;
        else{
            result = x*y;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(area(5.0));
        System.out.println(area(-1));
        System.out.println(area(5.0, 4.0));
        System.out.println(area(-1.0, 4.0));
    }
}
