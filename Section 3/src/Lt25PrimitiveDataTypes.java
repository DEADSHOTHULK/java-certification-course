public class Lt25PrimitiveDataTypes {
    public static void main(String args[]){

//        Width of 32
        int minIntValue = Integer.MIN_VALUE;
        int maxIntValue = Integer.MAX_VALUE;
        System.out.println("Minimum Int Value: " + minIntValue);
        System.out.println("Maximum Int Value: " + maxIntValue);
//        Overflow
        System.out.println("Busted max value: " + (maxIntValue+1));
//        Underflow
        System.out.println("Busted min value: " + (minIntValue-1));
        int literalValueWrittenWithUnderscore = 2_147_483_647;
        System.out.println("Test underscore value : " + literalValueWrittenWithUnderscore);

//        Width of 8
        byte minByteValue = Byte.MIN_VALUE;
        byte maxByteValue = Byte.MAX_VALUE;
        System.out.println("Minimum byte value : "+ minByteValue);
        System.out.println("Maximum byte value : "+ maxByteValue);

//        Width of 16
        short minShortValue = Short.MIN_VALUE;
        short maxShortValue = Short.MAX_VALUE;
        System.out.println("Minimum short value : "+ minShortValue);
        System.out.println("Maximum short value : "+ maxShortValue);

//        Width of 64
//        For small numbers L is not needed. But if a number exceeding int limit needs
//        to be typed then L is required
        long longValue = 1000L;
        long minLongValue = Long.MIN_VALUE;
        long maxLongValue = Long.MAX_VALUE;
        System.out.println("Minimum long value : "+ minLongValue);
        System.out.println("Maximum long value : "+ maxLongValue);

        int intTotal = (minIntValue / 2);
        byte byteTotal = (byte)(minByteValue / 2);
        short shortTotal = (short)(minShortValue / 2);

    }
}
