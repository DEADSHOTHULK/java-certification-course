public class Lt29FloatAndDouble {
    public static void main(String[] args) {

//        Width of 32
        float minFloatValue = Float.MIN_VALUE;
        float maxFloatValue = Float.MAX_VALUE;
        System.out.println("Min float value" + minFloatValue);
        System.out.println("Max float value" + maxFloatValue);

//        Width of 64
        double minDoubleValue = Double.MIN_VALUE;
        double maxDoubleValue = Double.MAX_VALUE;
        System.out.println("Min double value" + minDoubleValue);
        System.out.println("Max double value" + maxDoubleValue);

        int intVal = 5/3;
        float floatVal = 5f/3f;
        double doubleVal = 5d/3d;
        System.out.println("Int Value : " + intVal);
        System.out.println("Float Value : " + floatVal);
        System.out.println("Double Value : " + doubleVal);

    }
}
