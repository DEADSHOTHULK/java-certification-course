public class Lt28PrimitiveTypesChallenge {
    public static void main(String[] args) {

        byte byteVal = 90;
        short shortVal = 999;
        int intVal = 888888;
        long longVal = (50000*10) + byteVal + shortVal + intVal;
        System.out.println(longVal);
    }
}
