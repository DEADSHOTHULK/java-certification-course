public class Lt32PrimitiveTypesRecapAndString {
    public static void main(String[] args) {
//        byte
//        short
//        int
//        long
//        char
//        boolean
//        float
//        double

//        String is a class which is generally treated as a data type
//        Can take nay number of characters
//        Only restricted by the size of integer(for length calculation) and java heap space

//        String is immutable. Any updation only happens by creating a new string automatically
        String stringVar = "This is a string";
        System.out.println("Original string : "+stringVar);
        stringVar += " and this is more.";
        System.out.println("Updated string : "+stringVar);

        String numberString = "250.55";
        numberString+="49.95";
        System.out.println("Number string : "+numberString);

        int myInt = 10;
        numberString+=myInt;
        System.out.println("Number string updated val : "+numberString);
    }
}
