public class CodingExercise18 {
    public static boolean hasSharedDigit(int firstNum, int secondNum){
        boolean result = false;
        if(firstNum>=10 && firstNum<=99 && secondNum>=10 && secondNum<=99){
            label:while(firstNum>0){
                int currDigit = firstNum%10;
                int number = secondNum;
                while(number>0){
                    if(currDigit == (number%10)){
                        result = true;
                        break label;
                    }
                    number/=10;
                }
                firstNum/=10;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(hasSharedDigit(12,23));
        System.out.println(hasSharedDigit(9,99));
        System.out.println(hasSharedDigit(15,55));
    }
}
