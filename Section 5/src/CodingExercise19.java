public class CodingExercise19 {
    public static boolean hasSameLastDigit(int firstNum, int secondNum, int thirdNum){
        boolean result = false;
        if(isValid(firstNum) && isValid(secondNum) && isValid(thirdNum)){
            int lastDigitFirstNum = firstNum%10;
            int lastDigitSecondNum = secondNum%10;
            int lastDigitThirdNum = thirdNum%10;
            if(lastDigitFirstNum==lastDigitSecondNum ||
            lastDigitFirstNum==lastDigitThirdNum ||
            lastDigitSecondNum==lastDigitThirdNum){
                result = true;
            }
        }
        return result;
    }

    public static boolean isValid(int number){
        if(number>=10 && number<=1000)
            return true;
        return false;
    }

    public static void main(String[] args) {
        System.out.println(hasSameLastDigit(41,22,71));
        System.out.println(hasSameLastDigit(23,32,42));
        System.out.println(hasSameLastDigit(9,99,999));
        System.out.println(isValid(10));
        System.out.println(isValid(468));
        System.out.println(isValid(1051));
    }
}
