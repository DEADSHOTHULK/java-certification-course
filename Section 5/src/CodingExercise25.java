public class CodingExercise25 {
    public static int getLargestPrime(int number){
        int result = -1;
        if(number>=2){
            for(int i=2;i<=number;i++){
                if(number%i==0){
                    boolean isPrime = true;
                    for(int j=2;j<=(i/2);j++)
                        if(i%j==0){
                            isPrime = false;
                            break;
                        }
                    if(isPrime)
                        result = i;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getLargestPrime(7));
        System.out.println(getLargestPrime(31));
        System.out.println(getLargestPrime(0));
        System.out.println(getLargestPrime(45));
        System.out.println(getLargestPrime(-1));
    }
}
