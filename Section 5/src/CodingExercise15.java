public class CodingExercise15 {
    public static boolean isPalindrome(int number){
        int originalNumber = Math.abs(number);
        int reverseNumber = 0;
        while(originalNumber>0){
            reverseNumber = (reverseNumber*10)+ (originalNumber%10);
            originalNumber/=10;
        }
        if(Math.abs(number) == reverseNumber)
            return true;
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(-1221));
        System.out.println(isPalindrome(707));
        System.out.println(isPalindrome(11212));

    }
}
