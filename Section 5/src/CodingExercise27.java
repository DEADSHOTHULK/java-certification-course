import java.util.Scanner;
public class CodingExercise27 {
    public static void inputThenPrintSumAndAverage(){
        double sum = 0;
        long avg = 0;
        double count = 0;
        Scanner scanner = new Scanner(System.in);

        while(true){
            if(scanner.hasNextInt()){
                int number = scanner.nextInt();
                sum+=number;
                count++;
            }else{
                if(count!=0)
                    avg = Math.round((double)(sum/count));
                System.out.println("SUM = "+(int)sum+" AVG = "+avg);
                break;
            }
            scanner.nextLine();
        }
    }

    public static void main(String[] args) {
        inputThenPrintSumAndAverage();
        inputThenPrintSumAndAverage();
    }
}
