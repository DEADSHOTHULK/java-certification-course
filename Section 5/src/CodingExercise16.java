public class CodingExercise16 {
    public static int sumFirstAndLastDigit(int number){
        int result = -1;
        if(number>=0){
            result = number%10;
            int lastDigit = 0;
            while(number>0){
                if((number/10) == 0 && (number%10)>0) {
                    lastDigit = number % 10;
                    break;
                }
                number/=10;
            }
            result+=lastDigit;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(sumFirstAndLastDigit(0));
        System.out.println(sumFirstAndLastDigit(5));
        System.out.println(sumFirstAndLastDigit(-10));
    }
}
