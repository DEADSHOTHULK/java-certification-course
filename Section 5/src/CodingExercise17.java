public class CodingExercise17 {
    public static int getEvenDigitSum(int number){
        int sum = 0;
        if(number<0)
            sum = -1;
        else{
            while(number>0){
                int remainder = number%10;
                if(remainder%2==0)
                    sum+=remainder;
                number/=10;
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(getEvenDigitSum(123456789));
        System.out.println(getEvenDigitSum(252));
        System.out.println(getEvenDigitSum(-22));
    }
}
