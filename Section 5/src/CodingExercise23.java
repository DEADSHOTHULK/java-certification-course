public class CodingExercise23 {
    public static void numberToWords(int number){
        if(number<0)
            System.out.println("Invalid Value");
        else{
            int countOfDigits = getDigitCount(number);
            number = reverse(number);
            String wordsString = "";
            while(number>0){
                countOfDigits--;
                int remaining = number%10;
                number/=10;
                String digitText = "";
                switch (remaining){
                    case 0:
                        digitText = "Zero";
                        break;
                    case 1:
                        digitText = "One";
                        break;
                    case 2:
                        digitText = "Two";
                        break;
                    case 3:
                        digitText = "Three";
                        break;
                    case 4:
                        digitText = "Four";
                        break;
                    case 5:
                        digitText = "Five";
                        break;
                    case 6:
                        digitText = "Six";
                        break;
                    case 7:
                        digitText = "Seven";
                        break;
                    case 8:
                        digitText = "Eight";
                        break;
                    case 9:
                        digitText = "Nine";
                        break;
                    default:
                        digitText = "";
                        break;
                }
                wordsString+=(digitText+" ");
            }
            for(int i=0;i<countOfDigits;i++){
                wordsString+=("Zero ");
            }
            System.out.println(wordsString);
        }
    }

    public static int reverse(int number){
        int reversedNumber = 0;
        int absNumber = Math.abs(number);
        while(absNumber>0){
            reversedNumber = (reversedNumber*10) + (absNumber%10);
            absNumber/=10;
        }
        if(number<0)
            reversedNumber = -reversedNumber;
        return reversedNumber;
    }

    public static int getDigitCount(int number){
        int digitCount = 0;
        if(number==0)
            digitCount = 1;
        else if(number<0)
            digitCount = -1;
        else{
            while(number>0){
                digitCount++;
                number/=10;
            }
        }
        return digitCount;
    }

    public static void main(String[] args) {
        System.out.println(getDigitCount(0));
        System.out.println(getDigitCount(123));;
        System.out.println(getDigitCount(-12));
        System.out.println(getDigitCount(5200));
        System.out.println(reverse(-121));
        System.out.println(reverse(1212));
        System.out.println(reverse(1234));
        System.out.println(reverse(100));
        numberToWords(123);
        numberToWords(1010);
        numberToWords(1000);
        numberToWords(-12);
    }
}
