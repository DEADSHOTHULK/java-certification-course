public class CodingExercise14 {
    public static boolean isOdd(int number){
        boolean odd = false;
        if(number>0 && number % 2 == 1){
            odd  =true;
        }
        return odd;
    }

    public static int sumOdd(int startNum, int endNum){
        if(startNum<0 || endNum<0 || startNum>endNum)
            return -1;
        int sum = 0;
        for(int i=startNum;i<=endNum;i++){
            if(isOdd(i))
                sum+=i;
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(sumOdd(1,100));
        System.out.println(sumOdd(-1,100));
        System.out.println(sumOdd(100,100));
        System.out.println((sumOdd(13,13)));
        System.out.println(sumOdd(100,-100));
        System.out.println(sumOdd(100, 1000));
    }
}
