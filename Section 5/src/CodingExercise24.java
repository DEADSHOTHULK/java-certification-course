public class CodingExercise24 {
    public static boolean canPack(int bigCount, int smallCount, int goal){
        boolean result = false;
        int maxPossibleWeight = (bigCount*5) + (smallCount*1);
        if(bigCount<0 || smallCount<0 || goal<0)
            result = false;
        else if(goal > maxPossibleWeight)
            result = false;
        else if(smallCount > goal)
            result = true;
        else if((goal%5==0) && (goal/5)<=bigCount)
            result = true;
        else{
            int remaining = goal%5;
            int requiredBigCount = goal/5;
            if(requiredBigCount<=bigCount){
                if(remaining<=smallCount)
                    result = true;
            }else{
                remaining+=((bigCount-requiredBigCount)*5);
                if(remaining<=smallCount)
                    result = true;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(canPack(1,0,4));
        System.out.println(canPack(1,0,5));
        System.out.println(canPack(0,5,4));
        System.out.println(canPack(2,2,11));
        System.out.println(canPack(-3,2,12));
    }
}
