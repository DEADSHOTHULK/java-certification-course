public class CodingExercise22 {
    public static boolean isPerfectNumber(int number){
        boolean result = false;
        if(number>=1){
            int sumOfFactors = 0;
            for(int i=1;i<=(number/2);i++){
                if(number%i==0)
                    sumOfFactors+=i;
            }
            if(sumOfFactors==number)
                result = true;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(isPerfectNumber(6));
        System.out.println(isPerfectNumber(28));
        System.out.println(isPerfectNumber(5));
        System.out.println(isPerfectNumber(-1));
    }
}
