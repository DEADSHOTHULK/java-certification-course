public class CodingExercise20 {
    public static int getGreatestCommonDivisor(int first, int second){
        int gcd = -1;
        if(first>=10 && second>=10){
            int smallerNum = first<second?first:second;
            int largerNum = first>second?first:second;
            while(true){
                int remainder = largerNum%smallerNum;
                if(remainder==0){
                    gcd = smallerNum;
                    break;
                }
                largerNum = smallerNum;
                smallerNum = remainder;
            }
        }
        return gcd;
    }

    public static void main(String[] args) {
        System.out.println(getGreatestCommonDivisor(25, 15));
        System.out.println(getGreatestCommonDivisor(12,30));
        System.out.println(getGreatestCommonDivisor(9, 18));
        System.out.println(getGreatestCommonDivisor(81, 153));
    }
}
