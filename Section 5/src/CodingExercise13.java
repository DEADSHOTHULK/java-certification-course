public class CodingExercise13 {
    public static boolean isLeapYear(int year){
        boolean leapYear = false;
        if(year>=1 && year<=9999){
            if((year%4==0 && year%100!=0) || year%400==0)
                leapYear = true;
        }
        return leapYear;
    }

    public static int getDaysInMonth(int month, int year){
        int daysInMonth = 0;
        boolean leapYear = isLeapYear(year);
        switch (month){
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                daysInMonth = 31;
                break;
            case 2:
                if(leapYear)
                    daysInMonth = 29;
                else
                    daysInMonth = 28;
                break;
            case 4: case 6: case 9: case 11:
                daysInMonth = 30;
                break;
            default:
                daysInMonth = -1;
                break;
        }
        if(year<1 || year>9999)
            daysInMonth = -1;
        return daysInMonth;
    }

    public static void main(String[] args) {
        System.out.println(isLeapYear(-1600));
        System.out.println(isLeapYear(1600));
        System.out.println(isLeapYear(2017));
        System.out.println(isLeapYear(2000));
        System.out.println(getDaysInMonth(1,2020));
        System.out.println(getDaysInMonth(2,2020));
        System.out.println(getDaysInMonth(2,2018));
        System.out.println(getDaysInMonth(-1,2020));
        System.out.println(getDaysInMonth(1, -2020));
    }
}
