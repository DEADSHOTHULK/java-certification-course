package lecture97;

public class Main {
    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger("Basic","Sausage",3.56,"White");
        System.out.println(hamburger.itemizeHamburger());
        hamburger.addHamburgerAddition1("Tomato",0.27);
        hamburger.addHamburgerAddition2("Lettuce",0.75);
        hamburger.addHamburgerAddition3("Tomato",1.12);
        System.out.println("Total Burger price is "+hamburger.itemizeHamburger());

        HealthyBurger healthyBurger = new HealthyBurger("Bacon",5.67);
        healthyBurger.addHamburgerAddition1("Egg", 5.43);
        healthyBurger.addHealthyAddition5("Lentils",3.41);
        System.out.println("Total Healthy Burger price is "+healthyBurger.itemizeHamburger());

        DeluxeBurger deluxeBurger  =new DeluxeBurger();
        deluxeBurger.addHamburgerAddition1("Egg",5.43);
        System.out.println("Total Deluxe Burger price is "+deluxeBurger.itemizeHamburger());
    }
}
