package codingExercise35;

public class Circle {
    private double radius;
    public Circle(double radius){
        if(radius<0d)
            radius = 0;
        this.radius = radius;
    }

    public double getRadius(){
        return this.radius;
    }

    public double getArea(){
        double area = Math.PI * radius *radius;
        return area;
    }
}
