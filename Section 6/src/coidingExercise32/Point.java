package coidingExercise32;

public class Point {
    private int x;
    private int y;

    public Point(){
        this(0,0);
    }
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }

    public void setX(int x){
        this.x = x;
    }
    public void setY(int y){
        this.y = y;
    }

    public double distance(){
        double result = (x*x) + (y*y);
        return Math.sqrt(result);
    }
    public double distance(int x1, int y1){
        double result = Math.pow((x-x1), 2) + Math.pow((y-y1), 2);
        return Math.sqrt(result);
    }
    public double distance(Point point){
        int x1 = point.getX();
        int y1 = point.getY();
        return distance(x1, y1);
    }
}
