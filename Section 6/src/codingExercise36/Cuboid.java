package codingExercise36;

public class Cuboid extends Rectangle{
    private double height;
    public Cuboid(double width, double length, double height){
        super(length, width);
        if(height<0)
            height = 0;
        this.height = height;
    }

    public double getHeight(){
        return height;
    }
    public double getVolume(){
        double volume = getArea() * getHeight();
        return volume;
    }
}
