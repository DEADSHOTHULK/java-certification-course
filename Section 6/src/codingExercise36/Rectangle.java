package codingExercise36;

public class Rectangle {
    private double width;
    private double length;

    public Rectangle(double width, double length){
        if(length<0)
            length=0d;
        this.length = length;
        this.width = width<0?0d:width;
    }

    public double getWidth() {
        return this.width;
    }
    public double getLength(){
        return this.length;
    }

    public double getArea(){
        return (width*length);
    }
}
