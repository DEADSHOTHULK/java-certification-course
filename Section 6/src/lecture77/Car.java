package lecture77;

public class Car {
    private int doors;
    private int wheels;
    private String engine;
    private String model;
    private String color;

    public void setModel(String model){
        this.model = model;
    }

    public String getModel(){
        return model;
    }
}
