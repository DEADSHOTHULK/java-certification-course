Objects, like real world object have state and behaviour.
Objects depicts its state using its member variables or parameters
Objects expose their behaviour using its member methods.

Classes are just user-defined data types
Classes are the blueprint for the objects
Generally all the parameters in a class are private
Whenever a class is created. Some basic functionality get added automatically because a class inherits Java's base class.
